import json
import numpy as np
import math

def random_vector(length, mod):
    return np.random.randint(0, mod, length, dtype=np.int64)

def to_int_list(a):
    return [int(x) for x in a]

def poly_add(n, mod):
    a = random_vector(n, mod)
    b = random_vector(n, mod)
    c = a + b
    quotient = c // mod
    remainder = c - quotient * mod
    return {
        "a": to_int_list(a),
        "b": to_int_list(b),
        "c": to_int_list(remainder),
        "quotient": to_int_list(quotient),
        "modulus": mod
    }

def bitreverse(operand):
    operand = (((operand & (0xaaaaaaaa)) >> 1) | ((operand & (0x55555555)) << 1))
    operand = (((operand & (0xcccccccc)) >> 2) | ((operand & (0x33333333)) << 2))
    operand = (((operand & (0xf0f0f0f0)) >> 4) | ((operand & (0x0f0f0f0f)) << 4))
    operand = (((operand & (0xff00ff00)) >> 8) | ((operand & (0x00ff00ff)) << 8))
    return (operand >> 16) | (operand << 16)

def poly_mul(n, mod):
    a = random_vector(n, mod)
    b = random_vector(n, mod)
    c = np.zeros(n, dtype=np.int64)
    for i in range(n):
        for j in range(n):
            if i-j >= 0:
                c[i] += a[j] * b[i-j]
            else:
                c[i] -= a[j] * b[i-j+n]
    quotient = c // mod
    remainder = c - quotient * mod
    return {
        "a": to_int_list(a),
        "b": to_int_list(b),
        "c": to_int_list(remainder),
        "quotient": to_int_list(quotient),
        "modulus": mod
    }

def quick_exponent(x, exp, mod):
    ret = 1
    pow = x
    while exp>0:
        if (exp % 2) == 1:
            ret = (ret * pow) % mod
        pow = (pow * pow) % mod
        exp = exp // 2
    return ret

def get_root(n, mod):
    size_entire_group = mod - 1
    size_quotient_group = size_entire_group // n
    if size_entire_group - size_quotient_group * n != 0:
        assert(False)
    for r in range(mod):
        r = quick_exponent(r, size_quotient_group, mod)
        x = quick_exponent(r, n // 2, mod)
        print("tried", r, x)
        if x == mod-1: return r
    assert(False)

def poly_ntt(n, mod):
    arr = to_int_list(random_vector(n, mod))
    original = [i for i in arr]
    logn = int(math.log2(n))
    bit_reverses = [bitreverse(i) >> (32 - logn) for i in range(n)]
    root = get_root(n * 2, mod)
    powers = [1]
    for _ in range(1, n): powers.append(powers[-1] * root % mod)
    root_powers = [0 for _ in powers]
    for i in range(n):
        root_powers[bit_reverses[i]] = powers[i]
    m = 1
    layer = 0
    layered_arrs = []
    layered_vs = []
    layered_v_quotients = []
    layered_arr_quotients = []
    while m < n:
        layered_arrs.append([each for each in arr])
        gap = n // (m * 2)
        current_vs = []
        current_v_quotients = []
        current_arr_quotients = [0 for _ in range(n)]
        for id in range(n // 2):
            coeff_id = (id // gap * gap * 2) + (id % gap)
            root_id = m + (id // gap)
            r = root_powers[root_id]
            x = arr[coeff_id]
            y = arr[coeff_id + gap]
            u = x
            v = (y * r) % mod
            current_vs.append(v)
            current_v_quotients.append((y*r) // mod)
            arr[coeff_id] = (u + v) % mod
            arr[coeff_id + gap] = (u - v + mod) % mod
            current_arr_quotients[coeff_id] = (u + v) // mod
            current_arr_quotients[coeff_id + gap] = (u - v + mod) // mod
        layered_vs.append(current_vs)
        layered_v_quotients.append(current_v_quotients)
        layered_arr_quotients.append(current_arr_quotients)
        layer += 1
        m *= 2
    layered_arrs.append([each for each in arr])
    return {
        "original": original,
        "root_powers": root_powers,
        "arrays": layered_arrs,
        "vs": layered_vs,
        "v_quotients": layered_v_quotients,
        "array_quotients": layered_arr_quotients,
        "result": arr,
        "modulus": mod
    }


def save_json_file(obj, filename):
    with open(filename, "w") as fd:
        json.dump(obj, fd)

if __name__ == "__main__":
    np.random.seed(42)
    n = 1024
    modulus = 998244353
    save_json_file(poly_ntt(n, modulus), "input.json")