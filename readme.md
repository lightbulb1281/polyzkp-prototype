# ZKP system of polynomial ring Z[X]/(X^N+1)

## Requirements

* Install circom (requires Rust)
* Install snarkjs (using npm)

Make sure that when you run `snarkjs -h` and `circom -h` in terminal you could see help information.

## Run scripts

* First run `python gen.py` to generate `input.json`
* The scripts were written for Windows Powershell, but I believe they could be easily adapted to MacOS or Linux OS as well. Check `run.ps1`:
  * The first few lines of `powersoftaw` need only be run only once. After `pot16_final.ptau` is created you could comment these lines out.
  * The remaining lines are self-evident.
* Other scripts `verify/witness/compile.ps1` are literally a part of the `run.ps1` script.

## Notes

* When creating powers of tau, we used 16 (check the second line of `run.ps1` to ensure **there could be at least 2^16=65536 constraints** for NTT proof. If you use a smaller $n$ in NTT (currently 1024), this could be reduced. When you create the circuit (`circom circuit.circom`) you could see how many constraints are required.

## Other utilities

The `circuit.circom` file includes several usable components, you could change these, along with `gen.py` to produce other utilities:

#### 1. `template CheckMagicCube(n)`
ZKP for magic cube.

1. Change the line `component main ...` in `circuit.circom` to
    ```
    component main = CheckMagicCube(3);
    ```
2. Create/Edit `input.json` file with contents:
    ```json
    {
        "a": [
            [4, 9, 2],
            [3, 5, 7],
            [8, 1, 6]
        ]
    }
    ```
3. Run the script for proof generation and verification.
4. For bigger magic cubes, you need to change the `component main = CheckMagicCube(...)` and create the input json yourself. If the input is illegal, the proof could not be created.

#### 2. `template PolyAdd(n)`
ZKP for polynomial addition on ring $\mathbb{Z}_m[X]/(X^N+1)$
1. Change the line `component main ...` in `circuit.circom` to
    ```
    component main = PolyAdd(256); // or other n from your choice
    ```
2. Edit `gen.py` and run it to produce `input.json`:
    ``` python
    # from if __name__ == "__main__":
    n = 256 # or other n
    modulus = 998244353
    save_json_file(poly_add(n, modulus), "input.json")
    ```
3. Run the script for proof generation and verification.

#### 3. `template PolyMul(n)`
ZKP for polynomial multiplication on ring $\mathbb{Z}_m[X]/(X^N+1)$
1. Change the line `component main ...` in `circuit.circom` to
    ```
    component main = PolyMul(256); // or other n from your choice
    ```
2. Edit `gen.py` and run it to produce `input.json`:
    ``` python
    # from if __name__ == "__main__":
    n = 256 # or other n
    modulus = 998244353
    save_json_file(poly_mul(n, modulus), "input.json")
    ```
3. Run the script for proof generation and verification.


#### 4. `template PolyNTT(n)`
ZKP for polynomial NTT (number theory transform) on ring $\mathbb{Z}_m[X]/(X^N+1)$

**Note**: $n=1024$ need ~$2^{16}$ constraints and may be quite slow. Use $n=128$ and powersoftau to be only $2^{14}$ (edit all `16` to `14` in the first 7 lines of `run.ps1`) for a quick run.


1. Change the line `component main ...` in `circuit.circom` to
    ```
    component main {public [root_powers, result, modulus]} = PolyNTT(1024, 10); // or other n from your choice, the second parameter is log2(n).
    ```
2. Edit `gen.py` and run it to produce `input.json`:
    ``` python
    # from if __name__ == "__main__":
    n = 1024 # or other n
    modulus = 998244353
    save_json_file(poly_ntt(n, modulus), "input.json")
    ```
3. Run the script for proof generation and verification.

