# ZKP系统中多项式环的运算

# 1 重要性

同态加密作为一种特殊的加密方法，可以在不解密的情况下对密文进行计算比得到正确的结果，在隐私保护和数据安全上十分有用。

在近十年来兴起的由 Craig Gentry 提出的基于理想格的全同态加密系统上发展而来的基于 Ring Leanring-with-Error (RLWE) 问题的全同态密码系统，是隐私计算领域目前较为热门的技术之一。基于 RLWE 问题的全同态密码系统包括 BFV, BGV 以及 CKKS 等。BFV 和 BGV 密码系统支持对整数（域内整数）向量进行同态运算操作，包括向量加法和向量逐元素乘法。CKKS 密码系统则支持对复数向量进行近似同态操作。这三种密码系统的明密文空间均为多项式环 $R=\mathbb{Z}_q[X]/(X^N+1)$ 上的元素或其向量。以 CKKS 系统为例，CKKS 密码系统的消息空间为向量空间 $\mathbb{C}^{N/2}$，通过傅里叶变换，消息空间与明文空间 $R$ 在一定值域内同构。密文空间则为 $R^2$，基于 RLWE 问题将明文多项式 $m$ 隐藏在两个多项式 $c_1, c_2$ 中。

同态加密的运算高度依赖于多项式环上的元素的运算操作，即多项式加法和多项式乘法。使用ZKP对同态运算操作的过程进行证明，可以确保运算者忠实地执行了对应的同态操作，并不泄露输入数据的隐私。这对同态加密运算的正确性保证具有重要意义。下面本文尝试对多项式环上运算的ZKP证明进行实践。

# 2 多项式环的运算

考虑在环 $R=\mathbb{Z}_m[X]/(X^N+1)$ 上定义的多项式环。其中元素 $P=P(X)$ 可唯一地由系数 $a_i$ 表示为

$$
P(X) = \sum_{i=0}^{N-1}a_iX^i
$$

其次数不超过 $N-1$，且所有系数对 $q$ 取模。

可以定义加法与乘法

## 2.1 加法

$$
P=\sum_{i=0}^{N-1}a_iX_i,Q=\sum_{i=0}^{N-1}b_iX_i
$$

则两个多项式相加得到

$$
W=\sum_{i=0}^{N-1}(a_i+b_i)X_i
$$

注意，此处各项系数加法在域 $\mathbb{Z}_m$ 进行（即对 m 取模）。

## 2.2 乘法

同样，若对两多项式环上的元素进行乘法，则得到

$$
W(X) = P(X)Q(X) = \sum_{k=0}^{2N-1} \left(\sum_{i + j = k} a_i b_j\right)X_k
$$

由于需要对多项式 $X^N+1$ 取模，则上式等于

$$
W=\sum_{k=0}^{N-1}\left(\sum_{i=0}^{k}a_ib_{k-i}+\sum_{i=k+1}^{N-1}a_ib_{k-i+N}\right)X_k
$$

## 2.3 数论变换

注意到上述乘法运算需要 $O(N^2)$ 次乘法运算，而利用数论变换（Number Theory Transform, NTT），可以将多项式环上的元素（多项式）转换到向量空间 $\mathbb{Z}_q^N$ 上，此时向量的逐元素乘法对应原多项式环的多项式乘法。数论变换和逆数论变换均需要 $O(N\log N)$ 次乘法运算，则多项式乘法的复杂度下降到 $O(N\log N)$。

由于本文集中于零知识证明的实现和验证，此处略去数论变换的具体数学构造，其过程类似于快速傅里叶变换。即对于 $N$ 的元素的数论变换由 $l=\log_2 N$ 轮迭代构成。每轮迭代中，向量元素（即多项式系数）中的每两个元素发生交互操作，包含一次带模乘法。

# 3 实验步骤

## 3.1 环境搭建

我们采用 [snarkjs](https://github.com/iden3/snarkjs) 进行 ZKP 密钥、证明的生成。电路使用 [circom](https://docs.circom.io/getting-started/installation/) 进行编译。

## 3.2 多项式计算的电路实现

由于 circom 仅支持 R1CS 格式的约束生成，所有约束必须满足 $A\times B -C = 0$ 的形式，其中 $A,B,C$ 是输入信号的线性组合。这也意味着所有约束之多包含二次项。

然而值得注意的是，多项式环上的运算在域内进行，即需要对模数 $m$ 取模。这是 circom 的约束条件编译生成过程不能原生支持的。因此我们要求对于所有模运算 $a \bmod m=b$，必须输入三个数字 $a, b, q$，使得 circom 证明 $a-b=mq$ 即可。

上述要求使得所有带模运算必须多增加一个输入信号 $q$（整除商），这也不可避免地增大了输入输出信号个数和限制的数量。具体的实现代码参考 `circuit.circom` 文件中的 `template PolyAdd, template PolyMul, template PolyNTT` 实现。

在多项式乘法的实现中，由于需要进行 $N^2$ 次乘法，这也使得输入需要带上 $N^2$ 个整除商。而在 NTT 算法中，同样每层需要进行 $2N$ 次取模运算，这使得总的商的个数是 $2 N \log N$。

为了生成输入的信号，我们额外实现了 `gen.py`。使用 `python` 执行之，即可得到用于证明生成的输入文件。

## 3.3 实验生成证明并验证

### 3.3.1 预计算

1. 选择bn128曲线和最大支持 $2^{18}$ 约束的电路。
    ```bash
    snarkjs powersoftau new bn128 18 pot18_0000.ptau
    ```
2. 创建包含随机性的 powers of tau 文件，并由用户输入贡献随机性。
    ```bash
    snarkjs powersoftau contribute pot18_0000.ptau pot18_0001.ptau --name="Second contribution" -e="some random text"
    snarkjs powersoftau verify pot18_0001.ptau # 验证正确性
    ```
3. 使用Delayed Hash Function，应用随机的beacon
    ```bash
    snarkjs powersoftau beacon pot18_0001.ptau pot18_beacon.ptau 0102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f 10 -n="Final Beacon" # 执行了 $2^{10}$ 次的hash函数
    ```
4. 完成预计算，在这步及这步之前的结果可以被重复利用。
    ```bash
    snarkjs powersoftau prepare phase2 pot18_beacon.ptau pot18_final.ptau
    snarkjs powersoftau verify pot18_final.ptau # 验证正确性
    ```

### 3.3.2 电路搭建

```bash
circom circuit.circom --r1cs --wasm --sym
```

### 3.3.3 生成证明

1. 首先随机生成输入，得到文件 `input.json`
    ```bash
    python3 gen.py
    ```
2. 根据输入计算电路的连线中的witness
    ```bash
    node circuit_js/generate_witness.js circuit_js/circuit.wasm input.json witness.wtns
    ```
3. 生成 verification key
    ```bash
    snarkjs plonk setup circuit.r1cs pot18_final.ptau circuit_final.zkey
    snarkjs zkey export verificationkey circuit_final.zkey verification_key.json
    ```
4. 生成证明
    ```bash
    snarkjs plonk prove circuit_final.zkey witness.wtns proof.json public.json
    ```

### 3.3.4 验证证明

```bash
snarkjs plonk verify verification_key.json public.json proof.json
```

# 4 实验结果

对于多项式环实现了加法与乘法操作。

加法操作的电路复杂度为 $O(N)$、朴素的多项式乘法电路复杂度为$O(N^2)$，但是可以通过NTT算法来进行优化，得到复杂度为$O(N\log N)$的电路。本文并未实现完整的NTT优化乘法，仅实现NTT过程。

## 4.1 初始化

初始化花费了较长的时间，但是由于是一次性的操作，可以均摊到每一次的电路证明与验证中。

最终的 `pot${n}_final.ptau` 大小以及大致生成时间如下表所示：

| n    | 大小（Mb） | 时间（min） |
| ---- | ---------- | ----------- |
| 12   | 4.7        | 0.5         |
| 13   | 9.4        | 1           |
| 14   | 18.9       | 3           |
| 15   | 37.8       | 6           |
| 16   | 75.5       | 13          |
| 17   | 151        | 27          |
| 18   | 302        | 53          |

可以注意到生成电路的大小与时间与$2^n$成正比



## 4.2 加法

以 $N = 256$ 为例

从电路的编译结果中可以看到，对于 $n=256$ 的加法电路，总共有256个非线性约束，即256个乘法操作。1025个输入中，包括多项式a、b、c的各256个系数、得到的商多项式的256个系数和一个模数。

```
$ circom circuit.circom --r1cs --wasm --sym
template instances: 1
non-linear constraints: 256
linear constraints: 0
public inputs: 0
public outputs: 0
private inputs: 1025
private outputs: 0
wires: 1026
labels: 1026
Written successfully: ./circuit.r1cs
Written successfully: ./circuit.sym
Written successfully: ./circuit_js/circuit.wasm
Everything went okay, circom safe
```

在第二阶段，plonk生成了768个约束

```
$ snarkjs plonk setup circuit.r1cs pot18_final.ptau circuit_final.zkey
[INFO]  snarkJS: Reading r1cs
[INFO]  snarkJS: Plonk constraints: 768
[INFO]  snarkJS: Setup Finished

$ snarkjs zkey export verificationkey circuit_final.zkey verification_key.json
[INFO]  snarkJS: EXPORT VERIFICATION KEY STARTED
[INFO]  snarkJS: > Detected protocol: plonk
[INFO]  snarkJS: EXPORT VERIFICATION KEY FINISHED
snarkjs zkey export verificationkey circuit_final.zkey verification_key.json  
```

最后验证证明

```
$ snarkjs plonk verify verification_key.json public.json proof.json
[INFO]  snarkJS: OK!
```

### 效率结果

我们测量了当取不同N时证明生成、验证的耗时，以及生成的zkey所占用的空间。（使用`pot18_final.ptau` ）当N为64，128，256，512，1024时具体耗时及参数可参考如下：

| N    | 非线性约束个数 | plonk生成约束 | 证明生成耗时                                 | 验证耗时                                     | zkey容量 |
| ---- | -------------- | ------------- | -------------------------------------------- | -------------------------------------------- | -------- |
| 64   | 64             | 192           | 2.65s | 0.57s | 398k     |
| 128  | 128            | 384           | 3.03s | 0.59s | 794k     |
| 256  | 256            | 768           | 3.81s | 0.57s | 1.6M     |
| 512  | 512            | 1536          | 5.21s | 0.58s | 3.2M     |
| 1024 | 1024           | 3072          | 7.79s | 0.58s | 6.3M     |

可以注意到非线性约束个数与N相等，plonk生成约束个数为3N

## 4.3 朴素多项式乘法

以 $N = 256$ 为例

在朴素多项式乘法的电路中，电路中有131328个约束，因此在最开始的初始化中选择了18作为参数，因为 $2^{17} < 131328 < 2^{18}$。

同时可以从命令执行完后时间统计结果看出来，在证明的生成阶段中，时间开销较大，需要大量的计算资源。但是在验证过程中，时间开销较小，可以较快地完成验证。

```
$ circom circuit.circom --r1cs --wasm --sym
template instances: 2
non-linear constraints: 65792
linear constraints: 0
public inputs: 0
public outputs: 0
private inputs: 1025
private outputs: 0
wires: 66562
labels: 197634
Written successfully: ./circuit.r1cs
Written successfully: ./circuit.sym
Written successfully: ./circuit_js/circuit.wasm
Everything went okay, circom safe

$ snarkjs plonk setup circuit.r1cs pot18_final.ptau circuit_final.zkey
[INFO]  snarkJS: Reading r1cs
[INFO]  snarkJS: Plonk constraints: 131328
[INFO]  snarkJS: Setup Finished

$ snarkjs zkey export verificationkey circuit_final.zkey verification_key.json
[INFO]  snarkJS: EXPORT VERIFICATION KEY STARTED
[INFO]  snarkJS: > Detected protocol: plonk
[INFO]  snarkJS: EXPORT VERIFICATION KEY FINISHED

$ snarkjs plonk prove circuit_final.zkey witness.wtns proof.json public.json

$ snarkjs plonk verify verification_key.json public.json proof.json
[INFO]  snarkJS: OK!
```

### 效率结果

当N为64，128，256等参数时（N = 512时约束个数超过了`pot18_final.ptau`所能容纳的约数个数），具体耗时及相关参数可参考如下：

| N    | 非线性约束个数 | plonk生成约束 | 生成证明耗时                                     | 验证证明耗时                                 | zkey容量 |
| ---- | -------------- | ------------- | ------------------------------------------------ | -------------------------------------------- | -------- |
| 64   | 4160           | 8256          | 21.98s    | 0.58s | 25M      |
| 128  | 16512          | 32896         | 77.31s   | 0.57s | 100.1M   |
| 256  | 65792          | 131328        | 302.99s user  | 0.58s | 400.6M   |

非线性约束个数$N（N+1）$个，plonk生成约束个数共$N(2N+1)$



## 4.4 NTT算法

### N 较小情况

取 $N = 256$时

可以看出，相比起朴素的多项式乘法减少了大量的约束条件，使得电路变的简单。

从证明的生成和验证所消耗的时间看出，相比起朴素的多项式乘法，NTT算法可以极大地加快生成证明的速度。从原先需要超过300秒优化到了只需要200秒，在利用上了多核的情况下，实际花费的时间从111秒减少到了60秒，其次由于朴素乘法必须使用`pot18_final.ptau`，而NTT时只需使用`pot14_final.ptau`，节省了大量预生成的时间（由上文中的统计表可知，大约节省了约50min）。

另一方面，NTT算法生成的证明大小也相对较大（1.37G：25M）。

```
$ circom circuit.circom --r1cs --wasm --sym                                          
template instances: 2
non-linear constraints: 4096
linear constraints: 0
public inputs: 513
public outputs: 0
private inputs: 6656
private outputs: 0
wires: 7682
labels: 10242
Written successfully: ./circuit.r1cs
Written successfully: ./circuit.sym
Written successfully: ./circuit_js/circuit.wasm
Everything went okay, circom safe

$ snarkjs plonk setup circuit.r1cs pot18_final.ptau circuit_final.zkey
[INFO]  snarkJS: Reading r1cs
[INFO]  snarkJS: Plonk constraints: 10753
[INFO]  snarkJS: Setup Finished

$ snarkjs zkey export verificationkey circuit_final.zkey verification_key.json
[INFO]  snarkJS: EXPORT VERIFICATION KEY STARTED
[INFO]  snarkJS: > Detected protocol: plonk
[INFO]  snarkJS: EXPORT VERIFICATION KEY FINISHED

$ snarkjs plonk prove circuit_final.zkey witness.wtns proof.json public.json

$ snarkjs plonk verify verification_key.json public.json proof.json
[INFO]  snarkJS: OK!
```

### N 较大情况

取 $N = 1024$时

随着N的增加，在证明的生成与验证中所需要的时间也相应地增加。

生成的 `zkey` 超过了 21GB，使用了大量的空间。

```
$ circom circuit.circom --r1cs --wasm --sym
template instances: 2
non-linear constraints: 20480
linear constraints: 0
public inputs: 2049
public outputs: 0
private inputs: 32768
private outputs: 0
wires: 37890
labels: 50178
Written successfully: ./circuit.r1cs
Written successfully: ./circuit.sym
Written successfully: ./circuit_js/circuit.wasm
Everything went okay, circom safe

$ snarkjs plonk setup circuit.r1cs pot18_final.ptau circuit_final.zkey
[INFO]  snarkJS: Reading r1cs
[INFO]  snarkJS: Plonk constraints: 53249
[INFO]  snarkJS: Setup Finished

$ snarkjs zkey export verificationkey circuit_final.zkey verification_key.json
[INFO]  snarkJS: EXPORT VERIFICATION KEY STARTED
[INFO]  snarkJS: > Detected protocol: plonk
[INFO]  snarkJS: EXPORT VERIFICATION KEY FINISHED

$ snarkjs plonk prove circuit_final.zkey witness.wtns proof.json public.json

$ snarkjs plonk verify verification_key.json public.json proof.json
[INFO]  snarkJS: OK!
```

### 效率结果

当N为64，128，256，512，1024等参数时，具体耗时及相关参数可参考如下：

| N    | 非线性约束 | plonk生成约束 | 生成证明耗时                                         | 验证证明耗时                                 | zkey容量 |
| ---- | ---------- | ------------- | ---------------------------------------------------- | -------------------------------------------- | -------- |
| 64   | 768        | 2049          | 16.62s        | 0.58s | 90.2M    |
| 128  | 1792       | 4737          | 49.62s       | 0.60s | 348.1M   |
| 256  | 4096       | 10753         | 197.06s       | 0.60s | 1.37G    |
| 512  | 9216       | 24065         | 782.83s    | 0.62s | 5.42G    |
| 1024 | 20480      | 53249         | 3307.20s  | 0.65s | 21.58G   |

非线性约束个数$2Nlog_2(N)$个，plonk生成约束个数共$N*(5*log_2(N)+2)+1$



Ps：注意到改变初始电路参数后证明生成的时间和大小其实并未发生很大的改变

| N    | 初始电路约束参数 | 非线性约束 | plonk生成约束 | 生成证明耗时                                         | 验证证明耗时                                 | zkey容量 |
| ---- | ---------------- | ---------- | ------------- | ---------------------------------------------------- | -------------------------------------------- | -------- |
| 64   | 12               | 768        | 2049          | 16.64s        | 0.59s | 90.2M    |
| 128  | 13               | 1792       | 4737          | 49.56s       | 0.60s | 348.1M   |
| 256  | 14               | 4096       | 10753         | 198.38s     | 0.61s | 1.37G    |
| 512  | 15               | 9216       | 24065         | 781.74s   | 0.62s | 5.42G    |
| 1024 | 16               | 20480      | 53249         | 3319.26s  | 0.66s | 21.58G   |

