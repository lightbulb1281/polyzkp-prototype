pragma circom 2.0.0;

template CheckMagicCube(n) {
    signal input a[n][n];
    var k;
    k = n*(1+n*n)/2;

    // check rows
    signal rowSums[n];
    for (var i=0; i<n; i++) {
        var s = 0;
        for (var j=0; j<n; j++) s += a[i][j];
        rowSums[i] <-- s;
        (rowSums[i] - k) * (rowSums[i] - k) === 0;
    }

    // check columns
    signal columnSums[n];
    for (var i=0; i<n; i++) {
        var s = 0;
        for (var j=0; j<n; j++) s += a[j][i];
        columnSums[i] <-- s;
        (columnSums[i] - k) * (columnSums[i] - k) === 0;
    }

    // check diagonals
    signal diagonal0;
    var s = 0;
    for (var i=0; i<n; i++) s += a[i][i];
    diagonal0 <-- s;
    (diagonal0 - k) * (diagonal0 - k) === 0;

    signal diagonal1;
    s = 0;
    for (var i=0; i<n; i++) s += a[i][n-i-1];
    diagonal1 <-- s;
    (diagonal1 - k) * (diagonal1 - k) === 0;
}

/*
{
    "a": [
        [4, 9, 2],
        [3, 5, 7],
        [8, 1, 6]
    ]
}
*/

template ModAdd() {
    signal input a;
    signal input b;
    signal input c;
    signal input q;
    signal input m;
    a + b === m * q + c;
}


template PolyAdd(n) {
    signal input a[n];
    signal input b[n];
    signal input c[n];
    signal input quotient[n];
    signal input modulus;
    for (var i=0; i<n; i++) {
        a[i] + b[i] === modulus * quotient[i] + c[i];
    }
}

template Mul() {
    signal input a;
    signal input b; 
    signal output c;
    c <== a * b;
}

template PolyMul(n) {
    signal input a[n];
    signal input b[n];
    signal input c[n];
    signal input quotient[n];
    signal input modulus;
    component multiply_matrix[n][n];
    for (var i=0; i<n; i++) {
        for (var j=0; j<n; j++) {
            multiply_matrix[i][j] = Mul();
            multiply_matrix[i][j].a <== a[i];
            multiply_matrix[i][j].b <== b[j];
        }
    }
    for (var i=0; i<n; i++) {
        var t = 0;
        for (var j=0; j<n; j++) {
            if (i>=j){
                t += multiply_matrix[j][i-j].c;
            } else {
                t -= multiply_matrix[j][i-j+n].c;
            }
        }
        t === modulus * quotient[i] + c[i];
    }
}

template PolyNTT(n, logn) {
    signal input original[n];
    signal input root_powers[n];
    signal input arrays[logn + 1][n];
    signal input vs[logn][n\2];
    signal input v_quotients[logn][n\2];
    signal input array_quotients[logn][n];
    signal input result[n];
    signal input modulus;
    component yrmul[logn][n\2];
    var m = 1; var layer = 0;
    while (m < n) {
        var gap = n \ m \ 2;
        for (var i = 0; i < n\2; i++) {
            var coeff_id = (i \ gap * gap * 2) + (i % gap);
            var root_id = m + (i \ gap);
            // check v
            yrmul[layer][i] = Mul();
            yrmul[layer][i].a <== arrays[layer][coeff_id + gap];
            yrmul[layer][i].b <== root_powers[root_id];
            yrmul[layer][i].c === vs[layer][i] + v_quotients[layer][i] * modulus;
            // check arr
            arrays[layer+1][coeff_id] + array_quotients[layer][coeff_id] * modulus === arrays[layer][coeff_id] + vs[layer][i];
            arrays[layer+1][coeff_id + gap] + array_quotients[layer][coeff_id + gap] * modulus === arrays[layer][coeff_id] + modulus - vs[layer][i];
        }
        layer += 1;
        m *= 2;
    }
    // check last layer
    for (var i=0; i<n; i++) {
        arrays[layer][i] === result[i];
    }
}

component main {public [root_powers, result, modulus]} = PolyNTT(1024, 10);