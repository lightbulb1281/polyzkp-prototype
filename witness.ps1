echo "Create circuit"
circom circuit.circom --r1cs --wasm --sym

echo "Create witness"
cd circuit_js
node generate_witness.js circuit.wasm ../input.json ../witness.wtns
cd ..