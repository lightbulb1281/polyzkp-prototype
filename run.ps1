echo "Create powers of tau"
snarkjs powersoftau new bn128 16 pot16_0000.ptau
snarkjs powersoftau contribute pot16_0000.ptau pot16_0001.ptau --name="Second contribution" -e="some random text"
snarkjs powersoftau verify pot16_0001.ptau
snarkjs powersoftau beacon pot16_0001.ptau pot16_beacon.ptau 0102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f 10 -n="Final Beacon"
snarkjs powersoftau prepare phase2 pot16_beacon.ptau pot16_final.ptau
snarkjs powersoftau verify pot16_final.ptau


echo "Create circuit"
circom circuit.circom --r1cs --wasm --sym

echo "Create witness"
cd circuit_js
node generate_witness.js circuit.wasm ../input.json ../witness.wtns
cd ..

echo "Setup"
snarkjs plonk setup circuit.r1cs pot16_final.ptau circuit_final.zkey
snarkjs zkey export verificationkey circuit_final.zkey verification_key.json

echo "Generate proof"
snarkjs plonk prove circuit_final.zkey witness.wtns proof.json public.json

echo "Verify proof"
snarkjs plonk verify verification_key.json public.json proof.json